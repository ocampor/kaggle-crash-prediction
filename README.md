# Kaggle Crash Prediction

Code for Kaggle's competition Porto Seguro’s Safe Driver Prediction https://www.kaggle.com/c/porto-seguro-safe-driver-prediction

## Documentation
## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites
- Python 3.6 with PIP.

### Installing

## Running the tests
Include instructions to run your tests.

## Deployment
Include instructions to deploy your project.

## Contributing
Include the rules for project contribution.

### Pull Request Process
Include the rules for other members to create a PR.

## Versioning
Include what is the versioning convention that you are following.

## Authors
Include initial authors of the project.

## License
Under which licence is your project?
