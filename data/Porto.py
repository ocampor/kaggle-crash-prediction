import collections

import pandas as pd
import time
from sklearn.model_selection import train_test_split

from utils.df_utils import append_dummy_variables


class Porto:
    def __init__(self, p, seed):
        data = pd.read_csv("/home/ocampor/Workspace/crash-prediction/data/train.csv")
        test = pd.read_csv("/home/ocampor/Workspace/crash-prediction/data/test.csv")
        self.test_id = test["id"]
        X_train, y_train = self.prepare(data, "id", "target")
        self.test, _ = self.prepare(test, "id")
        self.train, self.dev = self.split(X_train, y_train, p, seed)

    def split(self, X, y, p, seed):
        # train_test_split receives a matrix with samples in rows
        X_train, X_test, y_train, y_test = train_test_split(X, y, train_size=p, random_state=seed)
        train = (X_train, y_train)
        dev = (X_test, y_test)
        return train, dev

    def prepare(self, data, id_label, y_label=None):
        data = data.set_index(id_label)

        not_useful = [
            'ps_car_10_cat', 'ps_calc_01', 'ps_calc_02', 'ps_calc_03',
            'ps_calc_05', 'ps_calc_06', 'ps_calc_07', 'ps_calc_08',
            'ps_calc_09', 'ps_calc_10', 'ps_calc_11', 'ps_calc_12',
            'ps_calc_13', 'ps_calc_14']
        data = data.drop(not_useful, axis=1)

        categorical = [
            'ps_ind_04_cat', 'ps_ind_05_cat', 'ps_car_01_cat',
            'ps_car_02_cat', 'ps_car_03_cat', 'ps_car_04_cat',
            'ps_car_05_cat', 'ps_car_06_cat', 'ps_car_07_cat',
            'ps_car_08_cat', 'ps_car_09_cat'
        ]
        for col in categorical:
            data[col] = data[col].astype("category")


        data, cat_labels = append_dummy_variables(data, data[categorical], None)
        data = data.drop(categorical, axis=1)

        ind_binary = [
            'ps_ind_06_bin', 'ps_ind_07_bin', 'ps_ind_08_bin',
            'ps_ind_09_bin', 'ps_ind_10_bin', 'ps_ind_11_bin',
            'ps_ind_12_bin', 'ps_ind_13_bin', 'ps_ind_16_bin',
            'ps_ind_17_bin', 'ps_ind_18_bin'
        ]
        data["ind_bin_sum"] = data[ind_binary].sum(axis=1)

        calc_binary = [
            'ps_calc_15_bin', 'ps_calc_16_bin', 'ps_calc_17_bin',
            'ps_calc_18_bin', 'ps_calc_19_bin', 'ps_calc_20_bin'
        ]
        data = data.drop(calc_binary, axis=1)

        data['ps_car_13_x_ps_reg_03'] = data['ps_car_13'] * data['ps_reg_03']

        if y_label is None:
            return data.as_matrix(), None

        y = data[y_label].as_matrix()
        X = data.drop(y_label, axis=1).as_matrix()

        return X, y

    def write_prediction(self, X: collections.Iterable, file_name=None):
        output = pd.DataFrame({
            "target": pd.Series(X),
            "id": pd.Series(self.test_id)
        })

        if file_name is None:
            file_name = "results-{id}.csv".format(id=int(time.time()))
        output.set_index(["id"]).to_csv(file_name)


if __name__ == "__main__":
    porto = Porto(0.7, 1)
    print(porto.train)