#!/bin/bash

echo -e "\e[1mInstalling G++ 4.9...\e[0m"
sudo apt-get install g++-4.9

PROJECT_DIR=`pwd`
LIBRARY_DIR=${HOME}/.tmp/xgboost

echo -e "\e[1mCloning LigthGBM repository...\e[0m"
git clone --recursive https://github.com/dmlc/xgboost ${LIBRARY_DIR}
mkdir ${LIBRARY_DIR}/build && cd ${LIBRARY_DIR}/build
echo -e "\e[1mCompiling LigthGBM with GPU flags...\e[0m"
cmake -D CMAKE_C_COMPILER=gcc-4.9 -D CMAKE_CXX_COMPILER=g++-4.9 -DUSE_CUDA=ON ..
make -j4

echo -e "\e[1mInstalling LightGBM python package...\e[0m"
source ${PROJECT_DIR}/.venv/bin/activate && cd ${LIBRARY_DIR}/python-package
python setup.py install

echo -e "\e[1mCleaning...\e[0m"
sudo rm -r ${LIBRARY_DIR}

echo -e "\e[1mFinished\e[0m"