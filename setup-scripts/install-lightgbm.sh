#!/bin/bash

echo -e "\e[1mInstalling OpenGL...\e[0m"
sudo apt-get install ocl-icd-libopencl1 opencl-headers clinfo
echo -e "\e[1mInstalling Libbost...\e[0m"
sudo apt-get install libboost-dev

PROJECT_DIR=`pwd`
LIBRARY_DIR=${HOME}/.tmp/lightgbm

echo -e "\e[1mCloning LigthGBM repository...\e[0m"
git clone --recursive https://github.com/Microsoft/LightGBM ${LIBRARY_DIR}
mkdir ${LIBRARY_DIR}/build && cd ${LIBRARY_DIR}/build
echo -e "\e[1mCompiling LigthGBM with GPU flags...\e[0m"
cmake -DUSE_GPU=1 ..
make -j4

echo -e "\e[1mInstalling LightGBM python package...\e[0m"
source ${PROJECT_DIR}/.venv/bin/activate && cd ${LIBRARY_DIR}/python-package
python setup.py install

echo -e "\e[1mCleaning...\e[0m"
sudo rm -r ${LIBRARY_DIR}

echo -e "\e[1mFinished\e[0m"