import numpy as np

from data import Porto
from models import LightGBM, Xgboost, Ensemble
from utils.accuracy_metrics import gini_normalized

data = Porto(0.85, 30)
print("The size of the train sample is : ", data.train[0].shape[0])
print("The size of the dev sample is : ", data.dev[0].shape[0])

lgb_params = {'tree_learner': 'feature', 'min_data': 368, 'max_bin': 255, 'bagging_freq': 45, 'nthread': 8,
              'colsample_bytree': 0.499606615, 'max_depth': 8, 'num_leaves': 15, 'learning_rate': 0.06701926258962201,
              'boosting': 'gbdt', 'objective': 'binary', 'bagging_fraction': 0.701122284, 'is_unbalance': True,
              'verbose': 0}

xgb_params = {'min_child_weight': 93, 'colsample_bytree': 0.808037886, 'max_depth': 12, 'eta': 0.039118100669360736,
              'booster': 'gbtree', 'silent': 1, 'objective': 'binary:logistic', 'subsample': 0.433470414,
              'gamma': 0.100884929, 'nthread': 8, 'lambda': 1}

train_params = {
    # Parameters for train method of booster
    'num_boost_round': 3000, 'early_stopping_rounds': 70
}

clf1 = LightGBM(lgb_params)
clf1 = clf1.train(*data.train, eval_set=data.dev, **train_params)

clf2 = Xgboost(xgb_params)
clf2 = clf2.train(*data.train, eval_set=data.dev, **train_params)

clf3 = Ensemble(models=[clf1, clf2])

y = clf3.predict(data.test)
scores = clf3.score(*data.dev, gini_normalized)

print("The accuracy for test data set is {} with a stddev of {}".format(np.mean(scores), np.std(scores)))

data.write_prediction(y.ravel().tolist())
