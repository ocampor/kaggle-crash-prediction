import time

import numpy as np

from data import Porto
from models import LightGBM, Ensemble, Xgboost
from utils.accuracy_metrics import gini_normalized


def get_random_decimal(start, end, base=1000000000):
    start = start * base
    end = end * base
    return np.random.randint(start, end) / base


def get_logaritmic_decimal(start, end):
    base = 10
    start = np.log(start) / np.log(base)
    end = np.log(end) / np.log(base)
    return base ** get_random_decimal(start, end)


def get_random_xgboost_params():
    params = dict()
    params["min_child_weight"] = np.random.randint(40, 100)
    params["colsample_bytree"] = get_random_decimal(0.4, 0.9)
    params["max_depth"] = np.random.randint(6, 20)
    params["eta"] = get_logaritmic_decimal(0.001, 0.09)
    params["booster"] = "gbtree"
    params["silent"] = 1
    params["objective"] = "binary:logistic"
    params["subsample"] = get_random_decimal(0.4, 0.9)
    params["gamma"] = get_random_decimal(0.1, 0.5)
    params["nthread"] = 8
    params["lambda"] = 1
    return params


def get_random_lightgdm_params():
    params = dict()
    params["verbosity"] = 0
    params["device"] = "gpu"
    params["tree_learner"] = "feature"
    params["min_data"] = np.random.randint(10, 1000)
    params["max_bin"] = np.random.randint(3, 50)
    params["bagging_freq"] = np.random.randint(5, 50)
    params["colsample_bytree"] = get_random_decimal(0.4, 0.9)
    params["max_depth"] = np.random.randint(6, 20)
    params["num_leaves"] = np.random.randint(3, 2 * params["max_depth"])
    params["learning_rate"] = get_logaritmic_decimal(0.001, 0.09)
    params["boosting"] = "gbdt"
    params["objective"] = "binary"
    params["bagging_fraction"] = get_random_decimal(0.4, 0.9)
    params["bagging_freq"] = np.random.randint(5, 50)
    params["is_unbalance"] = True
    return params


def get_traininig_params():
    return {
        # Parameters for train method of booster
        'num_boost_round': 3000, 'early_stopping_rounds': 70
    }


def randomized_search_cv():
    data = Porto(p=0.85, seed=30)
    test_file = open("experimentsv1.3.csv", "a")

    while True:
        time_stamp = int(time.time())
        lgb_params = get_random_lightgdm_params()
        xgb_params = get_random_xgboost_params()
        train_params = get_traininig_params()
        print(lgb_params)
        print(xgb_params)

        clf1 = Xgboost(xgb_params)
        clf1 = clf1.train(*data.train, eval_set=data.dev, **train_params)

        clf2 = LightGBM(lgb_params)
        clf2 = clf2.train(*data.train, eval_set=data.dev, **train_params)

        clf3 = Ensemble([clf1, clf2])

        scores = clf3.score(*data.dev, gini_normalized)

        print("Accuracy: %.4f (%.4f)" % (float(np.mean(scores)), float(np.std(scores))))
        string = str(time_stamp) + "^" + str(lgb_params) + "^" + str(xgb_params) + "^" + \
                 str(np.mean(scores)) + "^" + str(np.std(scores)) + '\n'
        test_file.write(string)
        test_file.flush()


if __name__ == "__main__":
    randomized_search_cv()



