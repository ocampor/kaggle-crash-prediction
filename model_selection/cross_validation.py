import numpy as np
from sklearn.model_selection import StratifiedKFold

from models import Model
from utils.accuracy_metrics import gini_normalized


def cross_validation(model: Model, X_train: np.array, y_train: np.array, n_splits=3, random_state=42,
                     splitter_class=StratifiedKFold, **train_params):
    # Initialize an empty lists
    scores = []
    models = []

    # Initialize KFold
    cv = splitter_class(n_splits=n_splits, random_state=random_state)

    for train_samples, dev_samples in cv.split(X_train, y_train):
        # Divide data sets given KFold samples
        eval_set = X_train[dev_samples], y_train[dev_samples]
        X = X_train[train_samples]
        y = y_train[train_samples]

        model = model.train(X, y, eval_set=eval_set, **train_params)
        score = model.score(X, y, scoring_function=gini_normalized)

        # Store results
        models.append(model)
        scores.append(score)

    # Concatenate results and reshape to needed shape
    scores = np.array(scores).reshape((len(scores), 1))

    return {
        "scores": scores,
        "models": models
    }
