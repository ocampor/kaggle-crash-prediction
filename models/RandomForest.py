from sklearn.ensemble import RandomForestRegressor

from data import Porto
from utils.accuracy_metrics import gini


class RandomForest:
    def __init__(self, X_train, y_train):
        self.model = self.train(X_train, y_train)

    @staticmethod
    def train(X, y):
        model = RandomForestRegressor(n_jobs=-1, verbose=True)
        model.fit(X, y)
        return model

    def score(self, X, y, f):
        actual = y
        predicted = self.predict(X)
        return f(actual, predicted)

    def predict(self, X):
        return self.model.predict(X)


if __name__ == "__main__":
    data = Porto(0.98, 30)
    rf = RandomForest(*data.train)
    print("The size of the train sample is : ", data.train.shape[0])
    print("The size of the train sample is : ", data.dev.shape[1])
    print("Training Gini score: ", rf.score(*data.train, gini))
    print("Testing Gini score: ", rf.score(*data.dev, gini))

    test_prediction = rf.predict(data.test)
    data.write_prediction(test_prediction)
