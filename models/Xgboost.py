from typing import Tuple

import numpy as np
import xgboost as xgb

from models import Model
from utils.accuracy_metrics import gini_normalized


class Xgboost(Model):
    def __init__(self, params):
        Model.__init__(self)
        self.params = params

    def train(self, X_train: np.array, y_train: np.array, eval_set: Tuple[np.array, np.array]=None,
              **train_params) -> Model:

        # Get training parameters because some libraries mutate its content
        n_rounds = train_params.get("num_boost_round", 100)
        early_stopping = train_params.get("early_stopping_rounds", 10)
        maximize = train_params.get("maximize", True)
        feval = train_params.get("feval", Xgboost.gini)

        # Convert np.array into xgb dataset which is required to train a XGBoost
        train = xgb.DMatrix(X_train, label=y_train)

        if eval_set is None:
            watchlist = [(train, "train")]
        else:
            dev = xgb.DMatrix(eval_set[0], label=eval_set[1])
            watchlist = [(train, 'train'), (dev, 'dev')]

        self._model = xgb.train(self.params, train, n_rounds, watchlist, maximize=maximize,
                                early_stopping_rounds=early_stopping, verbose_eval=50, feval=feval)
        return self

    def predict(self, X: np.array) -> np.array:
        if self._model is None:
            raise Exception("Model is not trained yet")

        d_matrix = xgb.DMatrix(X)
        prediction = self._model.predict(d_matrix, ntree_limit=self._model.best_ntree_limit)
        return prediction.reshape((X.shape[0], 1))

    def score(self, X: np.array, y: np.array, scoring_function) -> float:
        predicted = self.predict(X)
        return scoring_function(y, predicted)

    @staticmethod
    def gini(predicted: np.array, data_set: xgb.DMatrix) -> Tuple[str, float]:
        labels = data_set.get_label()
        gini_score = gini_normalized(labels, predicted)
        return "gini-normalized", gini_score
