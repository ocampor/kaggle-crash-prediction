import numpy as np
from sklearn.neural_network import MLPRegressor

from data import Porto
from utils.accuracy_metrics import gini_normalized


class NeuralNetwork:
    def __init__(self, X_train, y_train, seed):
        self.model = self.train(X_train, y_train)

    @staticmethod
    def train(X, y):
        model = MLPRegressor(hidden_layer_sizes=(40, 15, 5), learning_rate_init=0.001,
                             verbose=True, random_state=seed, tol=1e-10)
        model.fit(X, y)
        return model

    def score(self, X, y, f):
        actual = y
        predicted = self.predict(X)
        return f(actual, predicted)

    def predict(self, X):
        return self.model.predict(X)


if __name__ == "__main__":
    seed = 30
    p = 0.98
    data = Porto(p, seed)
    print("The size of the train sample is : ", data.train[0].shape[0])
    print("The size of the dev sample is : ", data.dev[0].shape[0])
    rf = NeuralNetwork(*data.train, seed)
    print("Training Gini score: ", rf.score(*data.train, gini_normalized))
    print("Testing Gini score: ", rf.score(*data.dev, gini_normalized))

    test_prediction = rf.predict(data.test)
    test_prediction = np.where(test_prediction < 0, 0, test_prediction)
    test_prediction = np.where(test_prediction > 1, 1, test_prediction)
    data.write_prediction(test_prediction)
