from typing import TypeVar, Tuple

import numpy as np
from sklearn.metrics import confusion_matrix

M = TypeVar('Model')


class Model:
    def __init__(self):
        self._model = None
        self.params = None

    def train(self, X_train: np.array, y_train: np.array, eval_set: Tuple[np.array, np.array]=None,
              **train_params) -> M:
        raise NotImplementedError

    def score(self, X: np.array, y: np.array, scoring_function) -> float:
        raise NotImplementedError

    def predict(self, X: np.array) -> np.array:
        raise NotImplementedError

    def get_model(self):
        return self._model

    def confusion_matrix(self, X: np.array, y: np.array) -> str:
        if self._model is None:
            raise Exception("Model is not trained yet")

        predicted = self.predict(X)
        predicted = np.round(predicted).ravel()
        return confusion_matrix(y, predicted)

