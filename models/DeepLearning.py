import numpy as np
import tensorflow as tf

from data import Porto
from utils.accuracy_metrics import gini_normalized


class DeepLearning:
    def __init__(self, X_train, y_train, **params):
        self.parameters = self.train(X_train, y_train, **params)

    @staticmethod
    def train(X_train, Y_train, keep_prob=1.0, learning_rate=0.001, num_epochs=10000, seed=1):
        tf.set_random_seed(seed)
        (m, n_x) = X_train.shape
        n_y = 1

        # Create Placeholders of shape (n_x, n_y)
        X, Y = DeepLearning.create_placeholders(n_x, n_y)
        # Initialize parameters
        parameters = DeepLearning.initialize_parameters(seed, n_x, n_y)
        # Forward propagation: Build the forward propagation in the tensorflow graph
        Z4 = DeepLearning.forward_propagation(X, parameters, keep_prob)
        # Cost function: Add cost function to tensorflow graph
        cost = DeepLearning.compute_cost(Z4, Y)
        # Backpropagation: Define the tensorflow optimizer. Use an AdamOptimizer.
        optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)
        # Initialize all the variables
        init = tf.global_variables_initializer()

        with tf.Session() as sess:
            # Run the initialization
            sess.run(init)
            # Do the training loop
            for epoch in range(num_epochs):
                _, epoch_cost = sess.run([optimizer, cost], feed_dict={X: X_train.T, Y: Y_train.reshape((1, m))})
                print("Cost after epoch %i: %f" % (epoch, np.mean(epoch_cost)))

            parameters = sess.run(parameters)

        return parameters

    @staticmethod
    def forward_propagation(X, parameters, keep_prob=1.0):
        """
        Implements the forward propagation for the model: LINEAR -> RELU -> LINEAR -> RELU -> LINEAR -> SOFTMAX

        Arguments:
        X -- input dataset placeholder, of shape (input size, number of examples)
        parameters -- python dictionary containing your parameters "W1", "b1", "W2", "b2", "W3", "b3"
                      the shapes are given in initialize_parameters

        Returns:
        Z3 -- the output of the last LINEAR unit
        """
        keep_prob = tf.constant(keep_prob, dtype=tf.float32)

        # Retrieve the parameters from the dictionary "parameters"
        W1 = parameters['W1']
        b1 = parameters['b1']
        W2 = parameters['W2']
        b2 = parameters['b2']
        W3 = parameters['W3']
        b3 = parameters['b3']
        W4 = parameters['W4']
        b4 = parameters['b4']

        Z1 = tf.matmul(W1, X) + b1
        A1 = tf.nn.relu(Z1)
        A1 = tf.nn.dropout(A1, keep_prob=keep_prob)
        Z2 = tf.matmul(W2, A1) + b2
        A2 = tf.nn.relu(Z2)
        A2 = tf.nn.dropout(A2, keep_prob=keep_prob)
        Z3 = tf.matmul(W3, A2) + b3
        A3 = tf.nn.relu(Z3)
        A3 = tf.nn.dropout(A3, keep_prob=keep_prob)

        return tf.matmul(W4, A3) + b4

    @staticmethod
    def compute_cost(Z4, Y):
        """
        Computes the cost

        Arguments:
        Z3 -- output of forward propagation (output of the last LINEAR unit), of shape (6, number of examples)
        Y -- "true" labels vector placeholder, same shape as Z3

        Returns:
        cost - Tensor of the cost function
        """
        logits = tf.transpose(Z4)
        labels = tf.transpose(Y)

        return tf.nn.sigmoid_cross_entropy_with_logits(logits=logits, labels=labels)

    @staticmethod
    def initialize_parameters(seed, n_x, n_y):
        tf.set_random_seed(seed)

        W1 = tf.get_variable("W1", [40, n_x], initializer=tf.contrib.layers.xavier_initializer(seed=seed))
        b1 = tf.get_variable("b1", [40, 1], initializer=tf.zeros_initializer())
        W2 = tf.get_variable("W2", [15, 40], initializer=tf.contrib.layers.xavier_initializer(seed=seed))
        b2 = tf.get_variable("b2", [15, 1], initializer=tf.zeros_initializer())
        W3 = tf.get_variable("W3", [5, 15], initializer=tf.contrib.layers.xavier_initializer(seed=seed))
        b3 = tf.get_variable("b3", [5, 1], initializer=tf.zeros_initializer())
        W4 = tf.get_variable("W4", [n_y, 5], initializer=tf.contrib.layers.xavier_initializer(seed=seed))
        b4 = tf.get_variable("b4", [n_y, 1], initializer=tf.zeros_initializer())

        return {
            "W1": W1,
            "b1": b1,
            "W2": W2,
            "b2": b2,
            "W3": W3,
            "b3": b3,
            "W4": W4,
            "b4": b4
        }

    @staticmethod
    def create_placeholders(n_x, n_y):
        """
        Creates the placeholders for the tensorflow session.

        Arguments:
        n_x -- scalar, size of an image vector (num_px * num_px = 64 * 64 * 3 = 12288)
        n_y -- scalar, number of classes (from 0 to 5, so -> 6)

        Returns:
        X -- placeholder for the data input, of shape [n_x, None] and dtype "float"
        Y -- placeholder for the input labels, of shape [n_y, None] and dtype "float"
        """
        X = tf.placeholder(tf.float32, [n_x, None])
        Y = tf.placeholder(tf.float32, [n_y, None])
        return X, Y

    def score(self, X, y, f):
        actual = y
        predicted = self.predict(X)
        return f(actual, predicted)

    def predict(self, X):
        W1 = tf.convert_to_tensor(self.parameters["W1"])
        b1 = tf.convert_to_tensor(self.parameters["b1"])
        W2 = tf.convert_to_tensor(self.parameters["W2"])
        b2 = tf.convert_to_tensor(self.parameters["b2"])
        W3 = tf.convert_to_tensor(self.parameters["W3"])
        b3 = tf.convert_to_tensor(self.parameters["b3"])
        W4 = tf.convert_to_tensor(self.parameters["W4"])
        b4 = tf.convert_to_tensor(self.parameters["b4"])

        params = {
            "W1": W1,
            "b1": b1,
            "W2": W2,
            "b2": b2,
            "W3": W3,
            "b3": b3,
            "W4": W4,
            "b4": b4
        }

        n_x = X.shape[1]
        x, _ = self.create_placeholders(n_x, 1)
        z4 = self.forward_propagation(x, params, 1.0)
        p = tf.nn.sigmoid(z4)

        sess = tf.Session()
        prediction = sess.run(p, feed_dict={x: X.T})
        return prediction.reshape(-1)


if __name__ == "__main__":
    seed = 30
    p = 0.98
    data = Porto(p, seed)
    print("The size of the train sample is : ", data.train[0].shape[0])
    print("The size of the dev sample is : ", data.dev[0].shape[0])

    params = {
        "keep_prob": 0.6,
        "learning_rate": 0.0001,
        "num_epochs": 10000,
        "seed": 1
    }
    rf = DeepLearning(*data.train, **params)
    print("Training Gini score: ", rf.score(*data.train, gini_normalized))
    print("Testing Gini score: ", rf.score(*data.dev, gini_normalized))
    test_prediction = rf.predict(data.test)
    data.write_prediction(test_prediction)
