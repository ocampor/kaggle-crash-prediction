from typing import Tuple

import lightgbm as lgb
import numpy as np

from models import Model
from utils.accuracy_metrics import gini_normalized


class LightGBM(Model):
    def __init__(self, params: dict):
        Model.__init__(self)
        self.params = params

    def train(self, X_train: np.array, y_train: np.array, eval_set: Tuple[np.array, np.array]=None,
              **train_params) -> Model:

        # Get training parameters because some libraries mutate its content
        n_rounds = train_params.get("num_boost_round", 100)
        early_stopping = train_params.get("early_stopping_rounds", 10)

        # Convert np.array into lgb data set which is required to train a XGBoost
        train = lgb.Dataset(X_train, label=y_train)

        if eval_set is None:
            dev = None
        else:
            dev = lgb.Dataset(eval_set[0], label=eval_set[1])

        self._model = lgb.train(self.params, train, num_boost_round=n_rounds, early_stopping_rounds=early_stopping,
                                valid_sets=dev, verbose_eval=100, feval=LightGBM.gini)
        return self

    def predict(self, X: np.array) -> np.array:
        if self._model is None:
            raise Exception("Model is not trained yet")

        prediction = self._model.predict(X, num_iteration=self._model.best_iteration)
        return prediction.reshape((X.shape[0], 1))

    def score(self, X: np.array, y: np.array, scoring_function) -> float:
        predicted = self.predict(X)
        return scoring_function(y, predicted)

    @staticmethod
    def gini(predicted: np.array, data_set: lgb.Dataset) -> Tuple[str, float, bool]:
        labels = data_set.get_label()
        gini_score = gini_normalized(labels, predicted)
        return "gini-normalized", gini_score, True
