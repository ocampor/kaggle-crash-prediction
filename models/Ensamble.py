from typing import List

import numpy as np

from models import Model


class Ensemble:
    def __init__(self, models: List[Model]):
        self._models: List[Model] = models

    def score(self, X: np.array, y: np.array, scoring_function: callable) -> List[float]:
        scores = []

        for model in self._models:
            predicted = model.predict(X)
            score = scoring_function(y, predicted)
            scores.append(score)

        return scores

    def predict(self, X: np.array) -> np.array:
        predictions = []

        for model in self._models:
            predictions.append(model.predict(X))

        new_data = np.concatenate(predictions, axis=1)
        new_data = np.mean(new_data, axis=1, keepdims=True)

        return new_data.reshape((X.shape[0], 1))
